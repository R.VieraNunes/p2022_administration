﻿namespace P2022_FSI_Administration.WinForm
{
    partial class AjouterClasse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjouterClasse));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.tbAEPrenom = new System.Windows.Forms.TextBox();
            this.lPrenom = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 87);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(232, 274);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(168, 77);
            this.bEnregistrer.TabIndex = 17;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click_1);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(20, 274);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(168, 77);
            this.bEffacer.TabIndex = 16;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            // 
            // tbAEPrenom
            // 
            this.tbAEPrenom.Location = new System.Drawing.Point(155, 159);
            this.tbAEPrenom.Name = "tbAEPrenom";
            this.tbAEPrenom.Size = new System.Drawing.Size(168, 20);
            this.tbAEPrenom.TabIndex = 15;
            // 
            // lPrenom
            // 
            this.lPrenom.AutoSize = true;
            this.lPrenom.Location = new System.Drawing.Point(50, 159);
            this.lPrenom.Name = "lPrenom";
            this.lPrenom.Size = new System.Drawing.Size(66, 13);
            this.lPrenom.TabIndex = 12;
            this.lPrenom.Text = "libelle classe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(114, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 28);
            this.label3.TabIndex = 30;
            this.label3.Text = "Ajouter une classe";
            // 
            // AjouterClasse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.tbAEPrenom);
            this.Controls.Add(this.lPrenom);
            this.Name = "AjouterClasse";
            this.Text = "AjouterClasse";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.TextBox tbAEPrenom;
        private System.Windows.Forms.Label lPrenom;
        private System.Windows.Forms.Label label3;
    }
}