﻿namespace P2022_FSI_Administration.WinForm
{
    partial class ModifierEtudiant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTelephone = new System.Windows.Forms.TextBox();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbClasse = new System.Windows.Forms.ComboBox();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.tbPrenom = new System.Windows.Forms.TextBox();
            this.tbAENom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lPrenom = new System.Windows.Forms.Label();
            this.lNom = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbTelephone
            // 
            this.tbTelephone.Location = new System.Drawing.Point(138, 226);
            this.tbTelephone.Name = "tbTelephone";
            this.tbTelephone.Size = new System.Drawing.Size(190, 20);
            this.tbTelephone.TabIndex = 47;
           // this.tbTelephone.TextChanged += new System.EventHandler(this.tbAETelephone_TextChanged);
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(138, 178);
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(190, 20);
            this.tbMail.TabIndex = 46;
          //  this.tbMail.TextChanged += new System.EventHandler(this.tbAEMail_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Téléphone";
          //  this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Mail";
         //   this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(67, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(236, 28);
            this.label3.TabIndex = 43;
            this.label3.Text = "Modifier un étudiant";
          //  this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // tbClasse
            // 
            this.tbClasse.DisplayMember = "libelleclasse";
            this.tbClasse.FormattingEnabled = true;
            this.tbClasse.Location = new System.Drawing.Point(138, 140);
            this.tbClasse.Name = "tbClasse";
            this.tbClasse.Size = new System.Drawing.Size(190, 21);
            this.tbClasse.TabIndex = 42;
            //this.tbClasse.SelectedIndexChanged += new System.EventHandler(this.cbClasse_SelectedIndexChanged);
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(182, 264);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(164, 40);
            this.bEnregistrer.TabIndex = 41;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(17, 264);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(159, 40);
            this.bEffacer.TabIndex = 40;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
          //  this.bEffacer.Click += new System.EventHandler(this.bEffacer_Click);
            // 
            // tbPrenom
            // 
            this.tbPrenom.Location = new System.Drawing.Point(138, 99);
            this.tbPrenom.Name = "tbPrenom";
            this.tbPrenom.Size = new System.Drawing.Size(190, 20);
            this.tbPrenom.TabIndex = 39;
         //   this.tbPrenom.TextChanged += new System.EventHandler(this.tbAEPrenom_TextChanged);
            // 
            // tbAENom
            // 
            this.tbAENom.Location = new System.Drawing.Point(138, 57);
            this.tbAENom.Name = "tbAENom";
            this.tbAENom.Size = new System.Drawing.Size(190, 20);
            this.tbAENom.TabIndex = 38;
         //   this.tbAENom.TextChanged += new System.EventHandler(this.tbAENom_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Classe";
           // this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lPrenom
            // 
            this.lPrenom.AutoSize = true;
            this.lPrenom.Location = new System.Drawing.Point(52, 102);
            this.lPrenom.Name = "lPrenom";
            this.lPrenom.Size = new System.Drawing.Size(43, 13);
            this.lPrenom.TabIndex = 36;
            this.lPrenom.Text = "Prénom";
            //this.lPrenom.Click += new System.EventHandler(this.lPrenom_Click);
            // 
            // lNom
            // 
            this.lNom.AutoSize = true;
            this.lNom.Location = new System.Drawing.Point(69, 60);
            this.lNom.Name = "lNom";
            this.lNom.Size = new System.Drawing.Size(29, 13);
            this.lNom.TabIndex = 35;
            this.lNom.Text = "Nom";
          //  this.lNom.Click += new System.EventHandler(this.lNom_Click);
            // 
            // ModifierEtudiant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 450);
            this.Controls.Add(this.tbTelephone);
            this.Controls.Add(this.tbMail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbClasse);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.tbPrenom);
            this.Controls.Add(this.tbAENom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lPrenom);
            this.Controls.Add(this.lNom);
            this.Name = "ModifierEtudiant";
            this.Text = "Form1";
         //   this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTelephone;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox tbClasse;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.TextBox tbPrenom;
        private System.Windows.Forms.TextBox tbAENom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lPrenom;
        private System.Windows.Forms.Label lNom;
    }
}