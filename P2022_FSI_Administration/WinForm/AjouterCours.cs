﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class AjouterCours : Form
    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        public AjouterCours()
        {
            InitializeComponent();
        }
        private void tbAENom_TextChanged(object sender, EventArgs e)
        {

        }

        private void bEffacer_Click_1(object sender, EventArgs e)
        {
            reInitialisation();
        }

        private void bEnregistrer_Click(object sender, EventArgs e)
        {
            // ajout des variables
            string nom = tbAENomCours.Text;
            string info = tbAEDescCours.Text;
            int idclasse = cbClasse.SelectedIndex;

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=OLEN@bts2022;";
                maConnexion = new NpgsqlConnection(connexion);

                maConnexion.Open();
                string insert = "INSERT INTO cours (libellecours, descriptioncours, idclasse) values (:nom, :info, :idclasse);";
                commande = new NpgsqlCommand(insert, maConnexion);
                commande.Parameters.Add(new NpgsqlParameter("nom", NpgsqlDbType.Varchar)).Value = nom;
                commande.Parameters.Add(new NpgsqlParameter("info", NpgsqlDbType.Varchar)).Value = info;
                // Ajout du parametre classe
                commande.Parameters.Add(new NpgsqlParameter("idclasse", NpgsqlDbType.Integer)).Value = idclasse+1;
                commande.Prepare();
                commande.CommandType = CommandType.Text;
                commande.ExecuteNonQuery();
                MessageBox.Show("Cours ajouté");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
            this.Close();
        }
        private void reInitialisation()
        {
            tbAENomCours.Text = "";
            tbAEDescCours.Text = "";
            cbClasse.SelectedIndex = 0;
        }

        private void bRetour_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AjouterCours_Load(object sender, EventArgs e)
        {
         

        }

        private void AjouterCours_Load_1(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter1.Fill(this.p2022_Appli_AdministrationDataSet1.classe);

        }
    }
}
