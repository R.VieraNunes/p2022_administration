﻿using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration
{
    public partial class ListeEtudiant : Form
    {
        Utilisateur uti;
        Section cla;
        Cours cou;

       
        public ListeEtudiant(Utilisateur uticonnecte)
        {
            InitializeComponent();
            uti = uticonnecte;
            Form formAccueil = new Accueil(uti);
            formAccueil.Close();
        }

        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();

        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
        }

        private void ListeEtudiant_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet3.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.etudiantTableAdapter1.Fill(this.p2022_Appli_AdministrationDataSet3.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.etudiantTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet.etudiant);

        }

        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                Form formListeClasse = new ListeClasse(cla);
                formListeClasse.Show();
            
        }

        private void ajouterUneClasseToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        }

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeCours = new ListeCours(cou);
            formListeCours.Show();
        }

        private void ajouterCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void dgvListeEtudiant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var cells = dgvListeEtudiant.Rows[e.RowIndex].Cells; // récupère l'objet de la cellule cliquée
            var id = (int)cells[0].Value;    // l'index de la cellule 
            var nom = (string)cells[1].Value;   // première valeur contenue dans la cellule
            var prenom = (string)cells[2].Value;
            var idclasse = (int)cells[3].Value;
            var mail = (string)cells[4].Value;
            var tel = (string)cells[5].Value;

            new ModifierEtudiant(id, nom, prenom, idclasse, mail, tel)
                .Show();
        }
    }
}