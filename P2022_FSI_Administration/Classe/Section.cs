﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    public class Section
    {
        private int idClasse;
        private string libelleClasse;


        public int IdClasse { get => idClasse; set => idClasse = value; }
        public string LibelleClasse { get => libelleClasse; set => libelleClasse = value; }

        public Section(string libelle)
        {
            this.libelleClasse = libelle;
        }
    }
}
