﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    
    public class Cours
    {
        private int idcours;
        private string libellecours;
        private string descriptioncours;
        private Section laClasse;

        public Cours(string libellecours, string descriptioncours, Section laClasse)
        {
            this.libellecours = libellecours;
            this.descriptioncours = descriptioncours;
            this.laClasse = laClasse;
        }

        public int IdCours { get => IdCours; set => IdCours = value; }
        public string LibelleCours { get => LibelleCours; set => LibelleCours = value; }
        public string DescriptionCours { get => DescriptionCours; set => DescriptionCours = value; }
    }
}